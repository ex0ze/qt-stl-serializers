# qt-stl-serializers

Header-only library that allows serialize STL data structures with Qt QDataStream

### Supported data structures

+ __Non-associative:__
    + `std::vector`
    + `std::list`
    + `std::string`
    + `std::deque`
    + `std::queue`
    + `std::stack`
    + `std::(unordered_)(multi)set`
+ __Associative:__
    + `std::(unordered_)(multi)map`
    + `std::pair`


### How to use
```c++
   #include <QCoreApplication>

   #include "serializers/string.hpp"
   #include "serializers/list.hpp"
   #include "serializers/map.hpp"
   #include "serializers/pair.hpp"

   using namespace std::string_literals;

   int main(int argc, char *argv[])
   {
       QCoreApplication a(argc, argv);
       // some complex data structure
       std::unordered_map<std::string, std::list<std::pair<int, std::string>>> map { {"aa", {{1, "11"s}, {2, "22"s}, {3, "33"s}}},
                                                                                     {"bb", {{-4, "-44"s}, {-5, "-55"s}, {-6, "-66"s}}},
                                                                                     {"cc", {{7, "77"s}, {8, "88"s}, {9, "99"s}}},
                                                                                     {"dd", {{-10, "-1010"s}, {-11, "-1111"s}, {-12, "-1212"s}}} };
       //buffer for serialized bytes
       QByteArray serialized_contents;
       //create a serializer, that will write into a byte array
       QDataStream serializer(&serialized_contents, QIODevice::WriteOnly);
       serializer << map; // serialization;
       //create a deserializer, that will read from a byte array
       QDataStream deserializer(&serialized_contents, QIODevice::ReadOnly);
       std::unordered_map<std::string, std::list<std::pair<int, std::string>>> map_2;
       deserializer >> map_2; // deserialization
       assert(map == map_2);
       return a.exec();
   }
```
### P.S.
__I don't recommend you to use std::queue and std::stack for serialization. Library actually supports serialization for std::queue and std::stack, but their
serialization is less efficient than others (because of additional copying overhead).__
 
