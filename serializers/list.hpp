#ifndef LIST_HPP
#define LIST_HPP

#include "impl/list_impl.hpp"

template <typename T>
static QDataStream& operator<<(QDataStream& ds, const std::list<T>& list)
{
    return serializers::impl::list::serialize_impl(ds, list);
}

template <typename T>
static QDataStream& operator>>(QDataStream& ds, std::list<T>& list)
{
    return serializers::impl::list::deserialize_impl(ds, list);
}

#endif // LIST_HPP
