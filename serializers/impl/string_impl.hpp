#ifndef STRING_IMPL_HPP
#define STRING_IMPL_HPP

#include <QDataStream>
#include <string>

namespace serializers {
namespace impl {
namespace string
{

static QDataStream& serialize_impl(QDataStream& ds, const std::string& str)
{
    ds << static_cast<quint32>(str.size());
    ds.writeRawData(str.data(), str.size());
    return ds;
}

static QDataStream& deserialize_impl(QDataStream& ds, std::string& str)
{
    quint32 len;
    ds >> len;
    if (len > 0)
    {
        str.resize(len);
        ds.readRawData(str.data(), len);
    }
    return ds;
}

} // namespace string
} // namespace impl
} // namespace serializers

#endif // STRING_IMPL_HPP
