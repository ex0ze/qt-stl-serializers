#ifndef PAIR_IMPL_HPP
#define PAIR_IMPL_HPP

#include <QDataStream>
#include <utility>

namespace serializers {
namespace impl {
namespace pair
{

template <typename First, typename Second>
static QDataStream& serialize_impl(QDataStream& ds, const std::pair<First, Second>& pair)
{
    ds << pair.first << pair.second;
    return ds;
}

template <typename First, typename Second>
static QDataStream& deserialize_impl(QDataStream& ds, std::pair<First, Second>& pair)
{
    ds >> pair.first >> pair.second;
    return ds;
}

} // namespace pair
} // namespace impl
} // namespace serializers

#endif // PAIR_IMPL_HPP
