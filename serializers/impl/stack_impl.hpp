#ifndef STACK_IMPL_HPP
#define STACK_IMPL_HPP

#include <QDataStream>
#include <stack>
#include <vector>

namespace serializers {
namespace impl {
namespace stack
{

template <typename T>
static QDataStream& serialize_impl(QDataStream& ds, const std::stack<T>& stack)
{
    ds << static_cast<quint32>(std::size(stack));
    // make copy for iterating over stack
    auto st { stack };
    while (!st.empty())
    {
        ds << st.top();
        st.pop();
    }
    return ds;
}

template <typename T>
static QDataStream& deserialize_impl(QDataStream& ds, std::stack<T>& stack)
{
    quint32 size;
    ds >> size;
    if (size > 0)
    {
        std::vector<T> temp;
        temp.resize(size);
        T value;
        for (quint32 i {0}; i < size; ++i)
        {
            ds >> value;
            temp[size - i - 1] = std::move(value);
        }
        for (auto&& v : temp)
            stack.push(std::move(v));
    }
    return ds;
}

} // namespace stack
} // namespace impl
} // namespace serializers

#endif // STACK_IMPL_HPP
