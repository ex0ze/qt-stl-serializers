#ifndef DEQUE_IMPL_HPP
#define DEQUE_IMPL_HPP

#include <QDataStream>
#include <deque>

namespace serializers {
namespace impl {
namespace deque
{

template <typename T>
static QDataStream& serialize_impl(QDataStream& ds, const std::deque<T>& deque)
{
    ds << static_cast<quint32>(std::size(deque));
    for (const auto& v : deque)
        ds << v;
    return ds;
}

template <typename T>
static QDataStream& deserialize_impl(QDataStream& ds, std::deque<T>& deque)
{
    quint32 size;
    ds >> size;
    if (size > 0)
    {
        T value;
        for (quint32 i {0}; i < size; ++i)
        {
            ds >> value;
            deque.emplace_back(std::move(value));
        }
    }
    return ds;
}

} // namespace deque
} // namespace impl
} // namespace serializers

#endif // DEQUE_IMPL_HPP
