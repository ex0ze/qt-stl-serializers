#ifndef QUEUE_IMPL_HPP
#define QUEUE_IMPL_HPP

#include <QDataStream>
#include <queue>

namespace serializers {
namespace impl {
namespace queue
{

template <typename T>
static QDataStream& serialize_impl(QDataStream& ds, const std::queue<T>& queue)
{
    ds << static_cast<quint32>(std::size(queue));
    // make copy for iterating over queue
    auto q { queue };
    while (!q.empty())
    {
        ds << q.front();
        q.pop();
    }
    return ds;
}

template <typename T>
static QDataStream& deserialize_impl(QDataStream& ds, std::queue<T>& queue)
{
    quint32 size;
    ds >> size;
    if (size > 0)
    {
        T value;
        for (quint32 i {0}; i < size; ++i)
        {
            ds >> value;
            queue.push(std::move(value));
        }
    }
    return ds;
}

} // namespace queue
} // namespace impl
} // namespace serializers

#endif // QUEUE_IMPL_HPP
