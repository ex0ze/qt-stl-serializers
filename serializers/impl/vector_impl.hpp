#ifndef VECTOR_IMPL_HPP
#define VECTOR_IMPL_HPP

#include <QDataStream>
#include <vector>

namespace serializers {
namespace impl {
namespace vector
{

template <typename T>
static QDataStream& serialize_impl(QDataStream& ds, const std::vector<T>& vector)
{
    ds << static_cast<quint32>(std::size(vector));
    for (const auto & e : vector)
        ds << e;
    return ds;
}

template <typename T>
static QDataStream& deserialize_impl(QDataStream& ds, std::vector<T>& vector)
{
    quint32 size;
    ds >> size;
    if (size > 0)
    {
        T value;
        vector.reserve(size);
        for (quint32 i {0}; i < size; ++i)
        {
            ds >> value;
            vector.emplace_back(std::move(value));
        }
    }
    return ds;
}

} // namespace vector
} // namespace impl
} // namespace serializers

#endif // VECTOR_IMPL_HPP
