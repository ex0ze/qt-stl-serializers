#ifndef LIST_IMPL_HPP
#define LIST_IMPL_HPP

#include <QDataStream>
#include <list>

namespace serializers {
namespace impl {
namespace list
{

template <typename T>
static QDataStream& serialize_impl(QDataStream& ds, const std::list<T>& list)
{
    ds << static_cast<quint32>(std::size(list));
    for (const auto& v : list)
        ds << v;
    return ds;
}

template <typename T>
static QDataStream& deserialize_impl(QDataStream& ds, std::list<T>& list)
{
    quint32 size;
    ds >> size;
    if (size > 0)
    {
        T value;
        for (quint32 i {0}; i < size; ++i)
        {
            ds >> value;
            list.emplace_back(std::move(value));
        }
    }
    return ds;
}

} // namespace list
} // namespace impl
} // namespace serializers

#endif // LIST_IMPL_HPP
