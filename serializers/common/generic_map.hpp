#ifndef GENERIC_MAP_HPP
#define GENERIC_MAP_HPP

#include <QDataStream>
#include <map>
#include <unordered_map>

namespace serializers {
namespace common {
namespace generic_map {

template <typename Container>
static QDataStream& serialize_impl(QDataStream& ds, const Container& map)
{
    ds << static_cast<quint32>(std::size(map));
    for (const auto & p : map)
        ds << p.first << p.second;
    return ds;
}

template <typename Container>
static QDataStream& deserialize_impl(QDataStream& ds, Container& map)
{
    quint32 size;
    ds >> size;
    if (size > 0)
    {
        typename Container::key_type first;
        typename Container::mapped_type second;
        for (quint32 i {0}; i < size; ++i)
        {
            ds >> first;
            ds >> second;
            map.emplace(std::move(first), std::move(second));
        }
    }
    return ds;
}

} // namespace generic_map
} // namespace common
} // namespace serializers

#endif // GENERIC_MAP_HPP
