#ifndef GENERIC_SET_HPP
#define GENERIC_SET_HPP

#include <QDataStream>
#include <set>
#include <unordered_set>

namespace serializers {
namespace common {
namespace generic_set {

template <typename Container>
static QDataStream& serialize_impl(QDataStream& ds, const Container& set)
{
    ds << static_cast<quint32>(std::size(set));
    for (const auto & v : set)
        ds << v;
    return ds;
}

template <typename Container>
static QDataStream& deserialize_impl(QDataStream& ds, Container& set)
{
         quint32 size;
         ds >> size;
         if (size > 0)
         {
             typename Container::value_type value;
             for (quint32 i {0}; i < size; ++i)
             {
                 ds >> value;
                 set.emplace(std::move(value));
             }
         }
         return ds;

}

} // namespace generic_set
} // namespace common
} // namespace serializers

#endif // GENERIC_SET_HPP
