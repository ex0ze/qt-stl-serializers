#ifndef ALL_HPP
#define ALL_HPP

#include "deque.hpp"
#include "list.hpp"
#include "map.hpp"
#include "pair.hpp"
#include "queue.hpp"
#include "set.hpp"
#include "stack.hpp"
#include "string.hpp"
#include "vector.hpp"

#endif // ALL_HPP
