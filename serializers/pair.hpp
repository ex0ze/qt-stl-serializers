#ifndef PAIR_HPP
#define PAIR_HPP

#include "serializers/impl/pair_impl.hpp"

template <typename First, typename Second>
static QDataStream& operator<<(QDataStream& ds, const std::pair<First, Second>& pair)
{
    return serializers::impl::pair::serialize_impl(ds, pair);
}

template <typename First, typename Second>
static QDataStream& operator>>(QDataStream& ds, std::pair<First, Second>& pair)
{
    return serializers::impl::pair::deserialize_impl(ds, pair);
}

#endif // PAIR_HPP
