#ifndef DEQUE_HPP
#define DEQUE_HPP

#include "impl/deque_impl.hpp"

template <typename T>
static QDataStream& operator<<(QDataStream& ds, const std::deque<T>& deque)
{
    return serializers::impl::deque::serialize_impl(ds, deque);
}

template <typename T>
static QDataStream& operator>>(QDataStream& ds, std::deque<T>& deque)
{
    return serializers::impl::deque::deserialize_impl(ds, deque);
}


#endif // DEQUE_HPP
