#ifndef STACK_HPP
#define STACK_HPP

#include "impl/stack_impl.hpp"

template <typename T>
static QDataStream& operator<<(QDataStream& ds, const std::stack<T>& stack)
{
    return serializers::impl::stack::serialize_impl(ds, stack);
}

template <typename T>
static QDataStream& operator>>(QDataStream& ds, std::stack<T>& stack)
{
    return serializers::impl::stack::deserialize_impl(ds, stack);
}

#endif // STACK_HPP
