#ifndef SET_HPP
#define SET_HPP

#include "common/generic_set.hpp"

//--------------------------------------------SET-------------------------------------------//
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator<<(QDataStream& ds, const std::set<Key>& set)                   //
{                                                                                           //
    return serializers::common::generic_set::serialize_impl(ds, set);                       //
}                                                                                           //
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator>>(QDataStream& ds, std::set<Key>& set)                         //
{                                                                                           //
    return serializers::common::generic_set::deserialize_impl(ds, set);                     //
}                                                                                           //
                                                                                            //
//--------------------------------------------MULTISET--------------------------------------//
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator<<(QDataStream& ds, const std::multiset<Key>& set)              //
{                                                                                           //
    return serializers::common::generic_set::serialize_impl(ds, set);                       //
}                                                                                           //
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator>>(QDataStream& ds, std::multiset<Key>& set)                    //
{                                                                                           //
    return serializers::common::generic_set::deserialize_impl(ds, set);                     //
}                                                                                           //
                                                                                            //
//--------------------------------------------UNORDERED SET---------------------------------//
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator<<(QDataStream& ds, const std::unordered_set<Key>& set)         //
{                                                                                           //
    return serializers::common::generic_set::serialize_impl(ds, set);                       //
}                                                                                           //
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator>>(QDataStream& ds, std::unordered_set<Key>& set)               //
{                                                                                           //
    return serializers::common::generic_set::deserialize_impl(ds, set);                     //
}                                                                                           //
                                                                                            //
//--------------------------------------------UNORDERED MULTISET----------------------------//
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator<<(QDataStream& ds, const std::unordered_multiset<Key>& set)    //
{                                                                                           //
    return serializers::common::generic_set::serialize_impl(ds, set);                       //
}                                                                                           //
                                                                                            //
template <typename Key>                                                                     //
static QDataStream& operator>>(QDataStream& ds, std::unordered_multiset<Key>& set)          //
{                                                                                           //
    return serializers::common::generic_set::deserialize_impl(ds, set);                     //
}                                                                                           //
                                                                                            //
//------------------------------------------------------------------------------------------//
#endif // SET_HPP
