#ifndef VECTOR_HPP
#define VECTOR_HPP

#include "impl/vector_impl.hpp"
#include <vector>

template <typename T>
static QDataStream& operator<<(QDataStream& ds, const std::vector<T>& vec)
{
    return serializers::impl::vector::serialize_impl(ds, vec);
}

template <typename T>
static QDataStream& operator>>(QDataStream& ds, std::vector<T>& vec)
{
    return serializers::impl::vector::deserialize_impl(ds, vec);
}

#endif // VECTOR_HPP
