#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "impl/queue_impl.hpp"

template <typename T>
static QDataStream& operator<<(QDataStream& ds, const std::queue<T>& queue)
{
    return serializers::impl::queue::serialize_impl(ds, queue);
}

template <typename T>
static QDataStream& operator>>(QDataStream& ds, std::queue<T>& queue)
{
    return serializers::impl::queue::deserialize_impl(ds, queue);
}

#endif // QUEUE_HPP
