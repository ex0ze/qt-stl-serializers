#ifndef MAP_HPP
#define MAP_HPP

#include "common/generic_map.hpp"

//--------------------------------------------MAP-------------------------------------------------//
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator<<(QDataStream& ds, const std::map<Key, Value>& map)                  //
{                                                                                                 //
    return serializers::common::generic_map::serialize_impl(ds, map);                             //
}                                                                                                 //
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator>>(QDataStream& ds, std::map<Key, Value>& map)                        //
{                                                                                                 //
    return serializers::common::generic_map::deserialize_impl(ds, map);                           //
}                                                                                                 //
                                                                                                  //
//--------------------------------------------MULTIMAP--------------------------------------------//
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator<<(QDataStream& ds, const std::multimap<Key, Value>& map)             //
{                                                                                                 //
    return serializers::common::generic_map::serialize_impl(ds, map);                             //
}                                                                                                 //
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator>>(QDataStream& ds, std::multimap<Key, Value>& map)                   //
{                                                                                                 //
    return serializers::common::generic_map::deserialize_impl(ds, map);                           //
}                                                                                                 //
                                                                                                  //
//--------------------------------------------UNORDERED MAP---------------------------------------//
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator<<(QDataStream& ds, const std::unordered_map<Key, Value>& map)        //
{                                                                                                 //
    return serializers::common::generic_map::serialize_impl(ds, map);                             //
}                                                                                                 //
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator>>(QDataStream& ds, std::unordered_map<Key, Value>& map)              //
{                                                                                                 //
    return serializers::common::generic_map::deserialize_impl(ds, map);                           //
}                                                                                                 //
                                                                                                  //
//--------------------------------------------UNORDERED MULTIMAP----------------------------------//
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator<<(QDataStream& ds, const std::unordered_multimap<Key, Value>& map)   //
{                                                                                                 //
    return serializers::common::generic_map::serialize_impl(ds, map);                             //
}                                                                                                 //
                                                                                                  //
template <typename Key, typename Value>                                                           //
static QDataStream& operator>>(QDataStream& ds, std::unordered_multimap<Key, Value>& map)         //
{                                                                                                 //
    return serializers::common::generic_map::deserialize_impl(ds, map);                           //
}                                                                                                 //
                                                                                                  //
//------------------------------------------------------------------------------------------------//
#endif // MAP_HPP
