#ifndef STRING_HPP
#define STRING_HPP

#include "impl/string_impl.hpp"

static QDataStream& operator<<(QDataStream& ds, const std::string& str)
{
    return serializers::impl::string::serialize_impl(ds, str);
}

static QDataStream& operator>>(QDataStream& ds, std::string& str)
{
    return serializers::impl::string::deserialize_impl(ds, str);
}

#endif // STRING_HPP
